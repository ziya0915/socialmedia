import java.util.Scanner;

public class loginpage {
	/**
	 * this is the implementation of the login method to login for the user
	 * here we use scanner to enter the username  and password 
	 * @param username
	 * @param password
	 * @author ziya
	 * @param reguserName 
	 * @param regpassword 
	 */
	public static void login() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the user_name (or) email:");
		String userName = sc.nextLine();
		System.out.println("Enter password :");
		String password = sc.nextLine();
		if(userName.equals("ziya")&& password.equals("123456")) {
		   System.out.println("login sucessfullyy...!!!");	
		}
		else {
			forgetpassword();
		}
	}
	
	/**
	 * this is the implementation of the forget password method to login for the user
	 * here we use scanner to enter the new password and  confirm password 
	 * @param new password
	 * @param confirm password
	 * @author ziya
	 */
	private static void forgetpassword() {
		Scanner sc = new Scanner(System.in);
		System.out.println("***Create a new password***");
		System.out.println("-----------------------");
		System.out.println("Enter new password :");
		String pswd1 = sc.nextLine();
		System.out.println("Confirm password :");
		String pswd2 = sc.nextLine();
		
		//checking weather the both passwords we enter are same or not
		
		if(pswd1==pswd2) {
			System.out.println("Password created.... ");
			login();
		}
		else {
			System.out.println("TRY AGAIN!!!!");
		}
}

}
